(function(){

  const PRODUCTS_LINK = 'http://my-json-server.typicode.com/achubirka/db/products';
  let app_el = document.getElementById('app');
  let products_list_el = app_el.querySelector('.js_products_list');
  let product_items = app_el.querySelectorAll('.js_product_item');
  let cart_el = app_el.querySelector('.js_shop_list');
  let total_el = app_el.querySelector('.js_total');

  let cart = [];

  function saveCart() {
    window.localStorage.setItem('shoppingCart', JSON.stringify(cart));
  }

  fetch(PRODUCTS_LINK).then((response) => response.json()).then((data) => {
    render(data);
  })

  function render(items) {
    let products_html = items.map(function(value, index) {
        return `
        <div class="js_product_item row" data-index="${index}"
                                         data-name="${value.name}"
                                         data-price="${value.price}"
                                         data-available="${value.available}">
          <div class="c5 js_item_text">
            ${value.name}
            ${value.price}$
          </div>
          <div class="c2 js_item_action">
            <button type="button" data-action="add" class="js_add add-button" ${(value.available < 1) ? "disabled" : ""}>ADD</button>
          </div>
        </div>
        <hr>
        `;
    }).join("");

    products_list_el.innerHTML = products_html;
    product_items = app_el.querySelectorAll('.js_product_item');
  }

function renderCart(price) {
  let cart_html = totalProductPrice().map(function(value, index) {
      return `
      <div class="js_cart_item row" data-index="${index}"
                                    data-name="${value.name}"
                                    data-price="${value.price}"
                                    data-available="${value.available}">
        ${value.name}
        ${value.total}$
        <button class="js_minus count-button" data-action="minus">-</button>
        ${value.count}
        <button class="js_plus count-button" data-action="plus" ${(value.available == value.count) ? "disabled" : ""}>+</button>
        <div class="c2 js_item_action">
          <button type="button" data-action="remove" class="js_remove remove-button">REMOVE</button>
        </div>
      </div>
      `;
  }).join("");

  cart_el.innerHTML = cart_html;
}

function addProduct(name, price, available) {
  for(var item in cart) {
       if(cart[item].name === name) {
         if(cart[item].count < cart[item].available) {
           cart[item].count ++;
         }
         return;
       }
     }
    cart.push({
        name: name,
        price: price,
        available: available,
        count: 1
      })

      saveCart();
}

function removeProductCount(name) {
  for(var item in cart) {
        if(cart[item].name === name) {
          cart[item].count --;
          if(cart[item].count === 0) {
            cart.splice(item, 1);
          }
          break;
        }
    }
    saveCart();
}

function addProductCount(name) {
  for(var item in cart) {
        if(cart[item].name === name) {
          if(cart[item].count < cart[item].available) {
            cart[item].count ++;
          }
        }
    }
    saveCart();
}

function totalProductPrice() {
  var cartCopy = [];
      for(i in cart) {
      item = cart[i];
      itemCopy = {};
      for(p in item) {
        itemCopy[p] = item[p];
      }
      itemCopy.total = Number(item.price * item.count).toFixed(2);
      cartCopy.push(itemCopy);
    }
    saveCart();
  return cartCopy;
}

function totalPrice() {
  var totalCart = 0;
   for(var item in cart) {
     totalCart += cart[item].price * cart[item].count;
   }
   total_el.innerText = Number(totalCart.toFixed(2)) + '$';
}

function removeAllProducts(name) {
  for(var item in cart) {
      if(cart[item].name === name) {
        cart.splice(item, 1);
        break;
      }
    }
    saveCart();
}

app_el.addEventListener('click', function(event) {
  let target = event.target;
  let product_item = target.closest('.js_product_item');
  let cart_item = target.closest('.js_cart_item');

  switch (target.dataset.action) {
    case 'add':
            index = Array.prototype.indexOf.call(product_items, product_item);
            addProduct(product_item.dataset.name, product_item.dataset.price, product_item.dataset.available);
            totalProductPrice();
            totalPrice();
            saveCart();
            renderCart();
            break;
    case 'remove':
            removeAllProducts(cart_item.dataset.name);
            totalPrice();
            saveCart();
            renderCart();
            break;
    case 'minus':
            removeProductCount(cart_item.dataset.name);
            totalProductPrice();
            totalPrice();
            saveCart();
            renderCart();
            break;
    case 'plus':
            addProductCount(cart_item.dataset.name);
            totalProductPrice();
            totalPrice();
            saveCart();
            renderCart();
            break;
    default:
        break;
  }
})

function restoreProducts() {
    const storedProducts = window.localStorage.getItem('shoppingCart');

    if (!storedProducts) return;

    for (const productData of JSON.parse(storedProducts)) {
        cart.push(productData);
        renderCart();
        totalPrice();
    }
}

window.addEventListener('storage', (event) => {
    if (event.key === 'shoppingCart') {
        cart = JSON.parse(event.newValue);
        renderCart();
        totalPrice();
      }
});

restoreProducts();

})();
