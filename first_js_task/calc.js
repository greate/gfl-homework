class Calculator {

  getSum(a, b) {
    return a + b;
  }

  getDiv(a, b) {
    return a / b;
  }

  getMult(a, b) {
    return a * b;
  }

  getSub(a, b) {
    return a - b;
  }

  getRemainder(a, b) {
    return a % b;
  }
}

var calc = new Calculator();

console.log(calc.getSum(2, 3));
console.log(calc.getDiv(8, 3));
console.log(calc.getMult(2, 3));
console.log(calc.getSub(5, 3));
console.log(calc.getRemainder(5, 2));
