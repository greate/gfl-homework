(function(){
  const USERS_LINK = 'https://jsonplaceholder.typicode.com/users';
  let app_el = document.getElementById('app');
  let todo_list_el = app_el.querySelector('.js_todo_list');
  let users = [];


  fetch(USERS_LINK).then((response) => response.json()).then((data) => {
    for (let i = 0; i < data.length; i++) {

      let tasks_html =  `
                 <li>`+data[i].name+ ' - ' + data[i].phone +`</li>
             `;

      todo_list_el.innerHTML += tasks_html;
    }
  })
})();
