$(document).ready(function() {

    const $users_app = $('#users_app');
    const $users_list = $users_app.find('.js_users_list');

    let comments = [{comments: 'HJJHKHJk'}];
    let sortedComments = [];

    let renderUser = makeRender('.js_user_template');

    function renderUsers() {
        let users_html = sortedComments.map(function(user) {
            return renderUser(user);
        }).join('');

        $users_list.html(users_html);
    }


    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/comments',
        method: 'GET',
        data: {},
        async: true,
    }).done(function(data) {
      comments = data.map(function(item) {
          return {
                name: item.name,
                post: item.postId
          };
      })
      validComments();
      renderUsers();
    })

    function validComments() {
      for(var item in comments) {
           if(comments[item].post === 15) {
             sortedComments.push({
               name: comments[item].name
             })
           }
         }
    }


    $users_app.on('click', '.js_user_name', function() {
        let $el = $(this);
        let index = $el.attr('data-id');
        console.log('select user id: ', index);

        $users_app.hide();
        $(document).trigger('show_todo', [index])
    });


    $(document).on('show_users', function() {
        $users_app.show();
    })


    renderUsers();

});
